const { isParenthesisMatching } = require('../src/string');

describe('String Balance Function', () => {
  it('number passed  Test will pass with value true ', () => {
    let str = 10;

    let answer = isParenthesisMatching(str);
    expect(typeof str).toBe('number');
    expect(typeof answer).toBe('boolean');
    expect(answer).toBe(true);
  });

  it('string passed }Hello) Test will pass with value false', () => {
    let str = '}Hello{';

    let answer = isParenthesisMatching(str);
    expect(typeof str).toBe('string');
    expect(typeof answer).toBe('boolean');
    expect(answer).toBe(false);
  });

  it('string passed {[(Hello]} Test will pass with value false', () => {
    let str = '{[(Hello]}';

    let answer = isParenthesisMatching(str);
    expect(typeof str).toBe('string');
    expect(typeof answer).toBe('boolean');
    expect(answer).toBe(false);
  });

  it('string passed ({Hello)} Test will pass with value false ', () => {
    let str = '({Hello)}';

    let answer = isParenthesisMatching(str);
    expect(typeof answer).toBe('boolean');
    expect(answer).toBe(false);
  });

  it('string passed {[(Hello {World(test)]} Test will pass with value false', () => {
    let str = '{[(Hello {World(test)]}';

    let answer = isParenthesisMatching(str);
    expect(typeof answer).toBe('boolean');
    expect(answer).toBe(false);
  });

  it('string passed {[(Hello {World(test)})]} Test will pass with value true', () => {
    let str = '{[(Hello {World(test)})]}';

    let answer = isParenthesisMatching(str);
    expect(typeof answer).toBe('boolean');
    expect(answer).toBe(true);
  });

  it('string passed Hello Test will pass with value true ', () => {
    let str = 'Hello';

    let answer = isParenthesisMatching(str);
    expect(typeof answer).toBe('boolean');
    expect(answer).toBe(true);
  });

  it('string passed {[(Hello)]} Test will pass with value true', () => {
    let str = '{[(Hello)]}';

    let answer = isParenthesisMatching(str);
    expect(typeof answer).toBe('boolean');
    expect(answer).toBe(true);
  });

  it('string passed {[(Hello {World}) nice]} Test will pass with value true', () => {
    let str = '{[(Hello {World}) nice]}';

    let answer = isParenthesisMatching(str);
    expect(typeof answer).toBe('boolean');
    expect(answer).toBe(true);
  });
});
