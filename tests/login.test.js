const request = require('supertest');
const app = require('../src/app');
const Login = require('../src/models/login');

test('should pass with 400 12 digit beacuse starts with (92) 929725453298 ', async () => {
  const response = await request(app)
    .post('/login')
    .send({
      mobilenumber: 929725453298,
      password: 'jayvirrathi'
    })
    .expect(400);
});

test('should pass with 400 only eleven digit 91972545329 ', async () => {
  const response = await request(app)
    .post('/login')
    .send({
      mobilenumber: 91972545329,
      password: 'jayvirrathi'
    })
    .expect(400);
});

test('should pass with 400 only nine digit 972545329 ', async () => {
  const response = await request(app)
    .post('/login')
    .send({
      mobilenumber: 972545329,
      password: 'jayvirrathi'
    })
    .expect(400);
});

test('should pass with 400 password length <6', async () => {
  const response = await request(app)
    .post('/login')
    .send({
      mobilenumber: 9725453298,
      password: '12345'
    })
    .expect(400);
});

test('should pass with 400  only nine digit 972545329 & password length <6 ', async () => {
  const response = await request(app)
    .post('/login')
    .send({
      mobilenumber: 972545329,
      password: '12345'
    })
    .expect(400);
});

test('should pass with 400 5725453298 ', async () => {
  const response = await request(app)
    .post('/login')
    .send({
      mobilenumber: 5725453298,
      password: '123456'
    })
    .expect(400);
});

test('should pass 919725453298 ', async () => {
  const response = await request(app)
    .post('/login')
    .send({
      mobilenumber: 919725453298,
      password: 'jayvirrathi'
    })
    .expect(201);
});

test('should pass +919725453298 ', async () => {
  const response = await request(app)
    .post('/login')
    .send({
      mobilenumber: +919725453298,
      password: 'jayvirrathi'
    })
    .expect(201);
});

test('should pass  6725453298 ', async () => {
  const response = await request(app)
    .post('/login')
    .send({
      mobilenumber: 6725453298,
      password: 'jayvirrathi'
    })
    .expect(201);
});

test('should pass  7725453298 ', async () => {
  const response = await request(app)
    .post('/login')
    .send({
      mobilenumber: 7725453298,
      password: 'jayvirrathi'
    })
    .expect(201);
});

test('should pass  8725453298 ', async () => {
  const response = await request(app)
    .post('/login')
    .send({
      mobilenumber: 8725453298,
      password: 'jayvirrathi'
    })
    .expect(201);
});

test('should pass  9725453298 ', async () => {
  const response = await request(app)
    .post('/login')
    .send({
      mobilenumber: 9725453298,
      password: '$%5jhksaz5'
    })
    .expect(201);
});

test('should login pass  with 400 mobilenumber length < 10 ', async () => {
  const response = await request(app)
    .post('/login/user')
    .send({
      mobilenumber: 97254598,
      password: 'jayvi'
    })
    .expect(400);
});

test('should  login pass with 400 password length < 6 ', async () => {
  const response = await request(app)
    .post('/login/user')
    .send({
      mobilenumber: 9725453298,
      password: 'jayvi'
    })
    .expect(400);
});

test('should login pass with 400 password length <6 & mobilenumber length <10  ', async () => {
  const response = await request(app)
    .post('/login/user')
    .send({
      mobilenumber: 91972548,
      password: 'jayi'
    })
    .expect(400);
});

test('should login pass  with 400 because of invalid ', async () => {
  const response = await request(app)
    .post('/login/user')
    .send({
      mobilenumber: 919725453298,
      password: 'jayvirdemo'
    })
    .expect(400);
});

test('should  login pass  with valid credentials ', async () => {
  const response = await request(app)
    .post('/login/user')
    .send({
      mobilenumber: 919725453298,
      password: 'jayvirrathi'
    })
    .expect(201);
});
