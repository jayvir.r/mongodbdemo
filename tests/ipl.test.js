const { iplTotalMatch } = require('../src/iplmatch');

describe('Total Matches Calculation', () => {
  it('float number  -3.4 passed Test will pass with value  ', () => {
    let totalTeam = -3.4;

    let totalMatches = iplTotalMatch(totalTeam);
    expect(typeof totalTeam).toBe('number');
    expect(typeof totalMatches).toBe('number');
    expect(totalMatches).toBe(0);
  });

  it('float number  -0.4 passed Test will pass with value  ', () => {
    let totalTeam = -0.4;

    let totalMatches = iplTotalMatch(totalTeam);
    expect(typeof totalTeam).toBe('number');
    expect(typeof totalMatches).toBe('number');
    expect(totalMatches).toBe(0);
  });

  it('float number  -0.0 passed Test will pass with value  ', () => {
    let totalTeam = -0.0;

    let totalMatches = iplTotalMatch(totalTeam);
    expect(typeof totalTeam).toBe('number');
    expect(typeof totalMatches).toBe('number');
    expect(totalMatches).toBe(0);
  });
  it('float number passed  0.4 Test will pass with value  ', () => {
    let totalTeam = 0.4;

    let totalMatches = iplTotalMatch(totalTeam);
    expect(typeof totalTeam).toBe('number');
    expect(typeof totalMatches).toBe('number');
    expect(totalMatches).toBe(0);
  });

  it('float number 4.4 passed Test will pass with value  ', () => {
    let totalTeam = 4.4;

    let totalMatches = iplTotalMatch(totalTeam);
    expect(typeof totalTeam).toBe('number');
    expect(typeof totalMatches).toBe('number');
    expect(totalMatches).toBe(6);
  });

  it('zero number passed Test will pass with value  ', () => {
    let totalTeam = 0;

    let totalMatches = iplTotalMatch(totalTeam);
    expect(typeof totalTeam).toBe('number');
    expect(typeof totalMatches).toBe('number');
    expect(totalMatches).toBe(0);
  });

  it('even 12 number passed Test will pass with value true ', () => {
    let totalTeam = 12;

    let totalMatches = iplTotalMatch(totalTeam);
    expect(typeof totalTeam).toBe('number');
    expect(typeof totalMatches).toBe('number');
    expect(totalMatches).toBe(66);
  });

  it('odd 15 number passed Test will pass with value  ', () => {
    let totalTeam = 15;

    let totalMatches = iplTotalMatch(totalTeam);
    expect(typeof totalTeam).toBe('number');
    expect(typeof totalMatches).toBe('number');
    expect(totalMatches).toBe(105);
  });

  it('string -15.5 passed Test will pass with value  ', () => {
    let totalTeam = '-15.5';

    let totalMatches = iplTotalMatch(totalTeam);
    expect(typeof totalTeam).toBe('string');
    expect(typeof totalMatches).toBe('number');
    expect(totalMatches).toBe(0);
  });

  it('string -15 passed Test will pass with value  ', () => {
    let totalTeam = '-15';

    let totalMatches = iplTotalMatch(totalTeam);
    expect(typeof totalTeam).toBe('string');
    expect(typeof totalMatches).toBe('number');
    expect(totalMatches).toBe(0);
  });

  it('string 0 passed Test will pass with value  ', () => {
    let totalTeam = '0';

    let totalMatches = iplTotalMatch(totalTeam);
    expect(typeof totalTeam).toBe('string');
    expect(typeof totalMatches).toBe('number');
    expect(totalMatches).toBe(0);
  });

  it('string 15 passed Test will pass with value  ', () => {
    let totalTeam = '15';

    let totalMatches = iplTotalMatch(totalTeam);
    expect(typeof totalTeam).toBe('string');
    expect(typeof totalMatches).toBe('number');
    expect(totalMatches).toBe(105);
  });
});

// test('Should pass  if first argument is negative', () => {
//   let num1 = -9,
//     num2 = 2;
//   var total = powerFun(num1, num2);
//   expect(typeof num1).toBe('number');
//   expect(typeof num2).toBe('number');
//   expect(Math.sign(num1)).toBe(-1);
//   expect(typeof total).toBe('number');
//   expect(total).toBe(81);
// });

// test('Should pass if second argument is negative', () => {
//   let num1 = 0.3,
//     num2 = -1;
//   var total = powerFun(num1, num2);
//   expect(typeof num1).toBe('number');
//   expect(typeof num2).toBe('number');
//   expect(Math.sign(num2)).toBe(-1);
//   expect(typeof total).toBe('number');
//   expect(total).toBe(3);
// });

// test('Should pass if both argument is negative', () => {
//   let num1 = -0.01,
//     num2 = -1;
//   var total = powerFun(num1, num2);
//   expect(typeof num1).toBe('number');
//   expect(typeof num2).toBe('number');
//   expect(Math.sign(num2)).toBe(-1);
//   expect(typeof total).toBe('number');
//   expect(total).toBe(-100);
// });

// test('Should pass if first argument is positive number', () => {
//   let num1 = 10,
//     num2 = 2;
//   var total = powerFun(num1, num2);
//   expect(total).toBe(100);
// });

// test('Should pass if second argument is positive number', () => {
//   let num1 = 10,
//     num2 = 2;
//   var total = powerFun(num1, num2);
//   expect(total).toBe(100);
// });

// test('Should pass both argument is positive number', () => {
//   let num1 = 10,
//     num2 = 2;
//   var total = powerFun(num1, num2);
//   expect(total).toBe(100);
// });

// test('Should fail if first argument is not a number', () => {
//   let num1 = '9',
//     num2 = 2;
//   var total = powerFun(num1, num2);
//   expect(typeof num1).toBe('string');
//   expect(typeof num2).toBe('number');
//   expect(typeof total).toBe('number');
//   expect(total).toBe(81);
// });

// test('Should fail if second argument is not a number', () => {
//   let num1 = 9,
//     num2 = '2';
//   var total = powerFun(num1, num2);
//   expect(typeof num1).toBe('number');
//   expect(typeof num2).toBe('string');
//   expect(typeof total).toBe('number');
//   expect(total).toBe(81);
// });

// test('Should fail if both are not a number', () => {
//   let num1 = '9',
//     num2 = '2';
//   var total = powerFun(num1, num2);
//   expect(typeof num1).toBe('string');
//   expect(typeof num2).toBe('string');
//   expect(typeof total).toBe('number');
//   expect(total).toBe(81);
// });
