const { powerFun } = require('../src/math');
test('Should pass  if first argument is negative', () => {
  let num1 = -9,
    num2 = 2;
  var total = powerFun(num1, num2);
  expect(typeof num1).toBe('number');
  expect(typeof num2).toBe('number');
  expect(Math.sign(num1)).toBe(-1);
  expect(typeof total).toBe('number');
  expect(total).toBe(81);
});

test('Should pass if second argument is negative', () => {
  let num1 = 0.3,
    num2 = -1;
  var total = powerFun(num1, num2);
  expect(typeof num1).toBe('number');
  expect(typeof num2).toBe('number');
  expect(Math.sign(num2)).toBe(-1);
  expect(typeof total).toBe('number');
  expect(total).toBe(3);
});

test('Should pass if both argument is negative', () => {
  let num1 = -0.01,
    num2 = -1;
  var total = powerFun(num1, num2);
  expect(typeof num1).toBe('number');
  expect(typeof num2).toBe('number');
  expect(Math.sign(num2)).toBe(-1);
  expect(typeof total).toBe('number');
  expect(total).toBe(-100);
});

test('Should pass if first argument is positive number', () => {
  let num1 = 10,
    num2 = 2;
  var total = powerFun(num1, num2);
  expect(total).toBe(100);
});

test('Should pass if second argument is positive number', () => {
  let num1 = 10,
    num2 = 2;
  var total = powerFun(num1, num2);
  expect(total).toBe(100);
});

test('Should pass both argument is positive number', () => {
  let num1 = 10,
    num2 = 2;
  var total = powerFun(num1, num2);
  expect(total).toBe(100);
});

test('Should fail if first argument is not a number', () => {
  let num1 = '9',
    num2 = 2;
  var total = powerFun(num1, num2);
  expect(typeof num1).toBe('string');
  expect(typeof num2).toBe('number');
  expect(typeof total).toBe('number');
  expect(total).toBe(81);
});

test('Should fail if second argument is not a number', () => {
  let num1 = 9,
    num2 = '2';
  var total = powerFun(num1, num2);
  expect(typeof num1).toBe('number');
  expect(typeof num2).toBe('string');
  expect(typeof total).toBe('number');
  expect(total).toBe(81);
});

test('Should fail if both are not a number', () => {
  let num1 = '9',
    num2 = '2';
  var total = powerFun(num1, num2);
  expect(typeof num1).toBe('string');
  expect(typeof num2).toBe('string');
  expect(typeof total).toBe('number');
  expect(total).toBe(81);
});
