const powerFun = (a, b) => {
  return parseInt(Math.pow(a, b));
};

module.exports = {
  powerFun
};
