const sgMail = require('@sendgrid/mail');

sgMail.setApiKey(process.env.SENDGRIP_API_KEY);

const sendWelcomeEmail = (email, name) => {
  sgMail.send({
    to: email,
    from: 'jayvir.r@innovify.in',
    subject: 'Thanks for joining in',
    text: `Welcome to app, ${name}. Let me know how you get along with app.`
  });
};

const sendGoodByeEmail = (email, name) => {
  sgMail.send({
    to: email,
    from: 'jayvir.r@innovify.in',
    subject: 'Sad to see you go',
    text: `Bye ${name} .Please provide us your feedback so that we can improve`
  });
};

module.exports = {
  sendWelcomeEmail,
  sendGoodByeEmail
};
