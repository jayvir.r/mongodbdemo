const express = require('express');
const Login = require('../models/login');
const router = new express.Router();

router.post('/login', async (req, res) => {
  const login = new Login(req.body);
  try {
    await login.save();
    res.status(201).send(login);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.post('/login/user', async (req, res) => {
  try {
    if (req.body.mobilenumber.length < 10) {
      return res.status(400).send({
        message: 'Mobile Number length less than 10'
      });
    }
    if (req.body.password.length < 6) {
      return res.status(400).send({
        message: 'Password length less than 6'
      });
    }

    var query = {
      mobilenumber: req.body.mobilenumber,
      password: req.body.password
    };

    const login = await Login.findOne(query);

    if (!login) {
      throw new Error();
    }
    res.status(201).send('Login Successful');
  } catch (e) {
    return res.status(400).send({
      message: 'invalid login'
    });
  }
});

module.exports = router;
