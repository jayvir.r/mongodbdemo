const mongoose = require('mongoose');
const loginSchema = new mongoose.Schema({
  mobilenumber: {
    type: String,
    trim: true,
    validate: {
      validator: function(v) {
        // var re = /^[6-9][0-9]{9}$/;
        var re = /^([+][9][1]|[9][1]|[0]){0,1}([6-9]{1})([0-9]{9})$/;

        return v == null || v.trim().length < 1 || re.test(v);
      },
      message: 'Provided phone number is invalid.'
    }
  },
  password: {
    type: String,
    required: true,
    minlength: 6,
    trim: true
  }
});

const Login = mongoose.model('Login', loginSchema);

module.exports = Login;
