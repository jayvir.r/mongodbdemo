const iplTotalMatch = a => {
  a = parseInt(a);
  if (a > 0) {
    return (a - 1) * (a / 2);
  } else {
    return 0;
  }
};

module.exports = {
  iplTotalMatch
};
